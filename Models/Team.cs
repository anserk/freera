using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace freera.Models
{
    public class TeamDbContext : DbContext
    {
        public DbSet<Team> Teams { get; set; }
    }

    public class Team
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<string> Members { get; set; }

        public Team(string name)
        {
            Name = name;
            Members = new List<string>();
        }

        public int AddMember(string name)
        {
            return -1;
        }

        public int RemoveMember(string name)
        {
            return -1;
        }
    }
}