using System.Threading.Tasks;

namespace freera.Repositories
{
    public interface ITeamsRepository
    {
         Task GetTeamsAsync();
         Task GetTeamAsync(int teamId);
         Task GetTeamByName(string name);
         Task GetMembersByTeam(int teamId);
         Task GetMemebersByTeamName(string name);
    }
}